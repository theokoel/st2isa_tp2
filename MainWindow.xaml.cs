﻿using System;
using System.Windows;

namespace TP2 {
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e) {

            var toDecrypt = ConvertCheckBox.IsChecked ?? false;
            var inputText = InputTextBox.Text;
            var encryptionmethod = EncryptionComboBox.Text;
            var optionText = OptionTextBox.Text;

            if (inputText == "") MessageBox.Show("Merci de remplir le champs d'entrée.", "Erreur", MessageBoxButton.OK, MessageBoxImage.Information);

            if (toDecrypt) OutputTextBox.Text = $"Decryptage du text en {encryptionmethod} en cours...";
            else OutputTextBox.Text = $"Cryptage du text en {encryptionmethod} en cours...";
            try {
                switch (encryptionmethod) {
                    case "Caesar":
                        if(optionText == "") MessageBox.Show("Merci de remplir le champs d'option.", "Erreur", MessageBoxButton.OK, MessageBoxImage.Information);
                        int optionInt = Int32.Parse(optionText);
                        OutputTextBox.Text = Caesar.Code(inputText, toDecrypt, optionInt);
                        break;
                    case "Substitution":
                        OutputTextBox.Text = Substitution.Code(inputText, toDecrypt);
                        break;
                    case "Vignere":
                        if(optionText == "") MessageBox.Show("Merci de remplir le champs d'option.", "Erreur", MessageBoxButton.OK, MessageBoxImage.Information);
                        OutputTextBox.Text = Vignere.Code(inputText, toDecrypt, optionText);
                        break;
                    default:
                        MessageBox.Show("La méthode de cryptage/decryptage séléctionné n'est pas valide.", "Erreur", MessageBoxButton.OK, MessageBoxImage.Information);
                        break;
                }
            } catch {
                MessageBox.Show("Une erreur inatendu c'est produit durant le processus de cryptage/decryptage.", "Erreur", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        internal static class Caesar {
            public static string Code(string inputText, bool toDecrypt, int key) {
                return toDecrypt ? Decipher(inputText, key) : Encipher(inputText, key);
            }
            private static char Cipher(char ch, int key) {
                if (!char.IsLetter(ch)) return ch;
                char offset = char.IsUpper(ch) ? 'A' : 'a';
                return (char)((((ch + key) - offset) % 26) + offset);
            }
            public static string Encipher(string input, int key) {
                string output = string.Empty;
                foreach (char ch in input) output += Cipher(ch, key);
                return output;
            }
            public static string Decipher(string input, int key) {
                return Encipher(input, 26 - key);
            }
        }

        internal static class Substitution {
            public static string Code(string inputText, bool toDecrypt) {
                string cipherAlphabet = "yhkqgvxfoluapwmtzecjdbsnri";
                string outText;
                bool encipherResult = toDecrypt ? Decipher(inputText, cipherAlphabet, out outText) : Encipher(inputText, cipherAlphabet, out outText);
                return outText;
            }
            private static bool Cipher(string input, string oldAlphabet, string newAlphabet, out string output) {
                output = string.Empty;
                if (oldAlphabet.Length != newAlphabet.Length) return false;
                for (int i = 0; i < input.Length; ++i) {
                    int oldCharIndex = oldAlphabet.IndexOf(char.ToLower(input[i]));
                    if (oldCharIndex >= 0) output += char.IsUpper(input[i]) ? char.ToUpper(newAlphabet[oldCharIndex]) : newAlphabet[oldCharIndex];
                    else output += input[i];
                }
                return true;
            }

            public static bool Encipher(string input, string cipherAlphabet, out string output) {
                string plainAlphabet = "abcdefghijklmnopqrstuvwxyz";
                return Cipher(input, plainAlphabet, cipherAlphabet, out output);
            }

            public static bool Decipher(string input, string cipherAlphabet, out string output) {
                string plainAlphabet = "abcdefghijklmnopqrstuvwxyz";
                return Cipher(input, cipherAlphabet, plainAlphabet, out output);
            }
        }

        internal static class Vignere {
            public static string Code(string inputText, bool toDecrypt, string key) {
                return toDecrypt ? Decipher(inputText, key) : Encipher(inputText, key);
            }

            private static int Mod(int a, int b) {
                return (a % b + b) % b;
            }

            private static string Cipher(string input, string key, bool encipher) {
                for (int i = 0; i < key.Length; ++i) if (!char.IsLetter(key[i])) return null;
                string output = string.Empty;
                int nonAlphaCharCount = 0;
                for (int i = 0; i < input.Length; ++i) {
                    if (char.IsLetter(input[i])) {
                        bool cIsUpper = char.IsUpper(input[i]);
                        char offset = cIsUpper ? 'A' : 'a';
                        int keyIndex = (i - nonAlphaCharCount) % key.Length;
                        int k = (cIsUpper ? char.ToUpper(key[keyIndex]) : char.ToLower(key[keyIndex])) - offset;
                        k = encipher ? k : -k;
                        char ch = (char)((Mod(((input[i] + k) - offset), 26)) + offset);
                        output += ch;
                    } else {
                        output += input[i];
                        ++nonAlphaCharCount;
                    }
                }
                return output;
            }

            public static string Encipher(string input, string key) {
                return Cipher(input, key, true);
            }

            public static string Decipher(string input, string key) {
                return Cipher(input, key, false);
            }

        }

        private void OnEncryptionComboBox(object sender, EventArgs e) {
            switch (EncryptionComboBox.Text)
            {
                case "Caesar":
                    OptionTextBox.Visibility = Visibility.Visible;
                    OptionTextBox.Text = "13";
                    break;
                case "Substitution":
                    OptionTextBox.Visibility = Visibility.Hidden;
                    break;
                case "Vignere":
                    OptionTextBox.Visibility = Visibility.Visible;
                    OptionTextBox.Text = "MyVignereKey";
                    break;
                default:
                    break;
            }
        }

    }
}
